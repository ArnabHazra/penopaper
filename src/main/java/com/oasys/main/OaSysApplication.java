package com.oasys.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class OaSysApplication {

	public static void main(String[] args) {
		SpringApplication.run(OaSysApplication.class, args);
	}

}

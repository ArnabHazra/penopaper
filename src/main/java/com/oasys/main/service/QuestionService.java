package com.oasys.main.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import com.oasys.main.model.Question;
import com.oasys.main.repository.QuestionRepository;

@Service
@EnableAsync

public class QuestionService {
	public static final String _1 = "1 * * * * *";
	@Autowired
	private QuestionRepository questionRepository;
	private static List<Question> questionBank;

	public List<Question> getQuestions() throws IOException {
		return generateQuestionThroughCSV();
	}

	@PostConstruct
	@Scheduled(cron = _1)
	public void setQuestionList() {
		
		
		questionBank = questionRepository.findAll();
		System.out.println(questionBank);
	}

	private List<Question> generateQuestionThroughCSV() throws IOException {
		List<Question> questionList = new ArrayList<>();
		Resource resource = null;
		BufferedReader bufferedReader = null;

		try {
			resource = new ClassPathResource("Book2.csv");
			InputStream inputStream = resource.getInputStream();
			String line = null;
			int itr = 0;
			bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = bufferedReader.readLine()) != null) {
				List<String> row = Arrays.asList(line.split(","));
				questionList.add(itr, new Question());
				questionList.get(itr).setQuestionType(row.get(1));
				questionList.get(itr).setQuestionText(row.get(2));
				questionList.get(itr).setQuestionDifficulty(row.get(3));
				questionList.get(itr).setOption1(row.get(4));
				questionList.get(itr).setOption2(row.get(5));
				questionList.get(itr).setOption3(row.get(6));
				questionList.get(itr).setOption4(row.get(7));
				List<String> correcOptions = Arrays.asList(row.get(8).split(";"));
				if (correcOptions.size() == 1) {
					if (correcOptions.get(0).contains("A"))
						questionList.get(itr).setCorrectOption(Arrays.asList(row.get(4)));

					if (correcOptions.get(0).contains("B"))
						questionList.get(itr).setCorrectOption(Arrays.asList(row.get(5)));

					if (correcOptions.get(0).contains("C"))
						questionList.get(itr).setCorrectOption(Arrays.asList(row.get(6)));

					if (correcOptions.get(0).contains("D"))
						questionList.get(itr).setCorrectOption(Arrays.asList(row.get(7)));
				} else {
					List<String> ops = new ArrayList<>();
					correcOptions.forEach(op -> {
						if (op.contains("A")) {
							ops.add((row.get(4)));
						}
						if (op.contains("B")) {
							ops.add((row.get(5)));
						}
						if (op.contains("C")) {
							ops.add((row.get(6)));
						}
						if (op.contains("D")) {
							ops.add((row.get(7)));
						}
					});
					questionList.get(itr).setCorrectOption(ops);

				}
				questionList.get(itr).setMarksAllocated(Integer.parseInt(row.get(9)));
				questionRepository.save(questionList.get(itr));

				itr++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bufferedReader.close();
		}
		return questionList;
	}

	public List<Question> getQuestionsFromDB() {
		return questionBank;
	}

	/*
	 * public Score verifyResponse(List<UserResponseDTO> userResponseDTO) {
	 * 
	 * List<Question> quesRes = getQuestionsFromDB().stream().parallel()
	 * .filter(ques -> userResponseDTO.stream().parallel() .anyMatch(res ->
	 * res.getResponse().equalsIgnoreCase(ques.getCorrectOption()) &&
	 * ques.getQuestionId() == Integer.parseInt(res.getResponseId())))
	 * .collect(Collectors.toList()); Score score = new Score(); Integer total =
	 * quesRes.size(); Integer numQues = userResponseDTO.size();
	 * score.setNumberOfQuestionsAttended(numQues); score.setTotalScore(total);
	 * score.setPercentage((total.doubleValue() / getQuestionsFromDB().size()) *
	 * 100); return score;
	 * 
	 * }
	 */

}
